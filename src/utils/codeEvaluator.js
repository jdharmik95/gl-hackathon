const problemsCollection = require('../database/mocks/problemsCollection'); // For simplicity of the implementation, I'm fetching the problemCollection from mockData from a file here, But ideally this will be a collection in document-based database like mongodb.
module.exports = async function(problemId, code){

    let problem = problemsCollection[problemId];
    if(!problem){
        throw new Error('Invalid problem given.');
    }
    let givenSolutionFun = null;
    eval(`givenSolutionFun=${code}`);
    
    let computedScore = 0;
    for(let testcase of problem['testCases']){
        try{
            let result = givenSolutionFun(testcase['input']);
            if(result==testcase['expectedOutput']){
                computedScore = computedScore + (testcase['weightage']*problem['maxScore']);
            }
        }catch(err){
            throw err;
        }
    }
    return computedScore;
}