const express = require('express');
const bodyParser = require('body-parser');
const hackathonRoutes = require('./controller/hackathon');

const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// apikey check middleware
app.use(function(req, res, next){
    if(req.header('apikey')!='DHv6bDPs5FcOdAhxYpRs'){
        res.status(401).json({
            error: 'Unauthorized request. Invalid accessToken given'
        });
        return;
    }
    next();
});

app.post(`/api/execute-code`,hackathonRoutes.evaluateCode);
app.get(`/api/contest/:contestId/leaderboard`,hackathonRoutes.getLeaderboard);
app.post(`/api/submissions`,hackathonRoutes.submissions);

// global error handler middleware
app.use(function(err, req, res, next){
    console.log('error handler middleware');
    console.log(err);
    next();
});

module.exports = app;
