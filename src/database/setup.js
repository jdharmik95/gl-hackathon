const sequelize = require('./sequelize');
const mockUsers = require('./mocks/users');
const mockGroups = require('./mocks/groups');
const mockGroupInvites = require('./mocks/groupInvites');
const mockContestGroups = require('./mocks/contestGroups');
const mockContestGroupSubmissions = require('./mocks/contestGroupSubmissions');

async function reset() {
	console.log('Will rewrite the SQLite example database, adding some dummy data.');

	await sequelize.sync({ force: true });

	await sequelize.models.user.bulkCreate(mockUsers);
	await sequelize.models.group.bulkCreate(mockGroups);
	let uniqueKeys = {};
	let newGroupInvites = [];
	for(let obj of mockGroupInvites){
		let key = `${obj.contestId}_${obj.groupId}`;
		if(!uniqueKeys[key]){
			newGroupInvites.push(obj);
			uniqueKeys[key] = 1;
		}
	}
	await sequelize.models.groupInvite.bulkCreate(newGroupInvites);
	await sequelize.models.contest.bulkCreate([{
		name: 'Great learning hiring Test',
		problemId: 1,
		description: 'This is the hackathon conducted for hiring interns to the great learning team.',
		startAtUTC: '2020-09-03 12:45:10.543 +00:00',
		endAtUTC: '2020-09-05 12:45:10.543 +00:00'
	}]);

	uniqueKeys = {};
	newContestGroups = [];
	for(let obj of mockContestGroups){
		let key = `${obj.contestId}_${obj.groupId}`;
		if(!uniqueKeys[key]){
			newContestGroups.push(obj);
			uniqueKeys[key] = 1;
		}
	}
	console.log(newContestGroups.length);
	await sequelize.models.contestGroup.bulkCreate(newContestGroups);
	await sequelize.models.contestGroupSubmission.bulkCreate(mockContestGroupSubmissions);
}

reset();
