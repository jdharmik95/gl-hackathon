module.exports = {
    1: {
        name: 'Print nth number in fibannoci series',
        description: 'Write a function int fib(int n) that returns Fn. For example, if n = 0, then fib() should return 0. If n = 1, then it should return 1. For n > 1, it should return Fn-1 + Fn-2',
        maxScore: 100,
        solutions: {
            'nodejs': 'function(n){\nlet a = 0;\nlet b = 1;\nif(n==0){\nreturn a;\n}else if(n==1){\nreturn b;\n}else{\nfor(let i=2;i<n+1;i++){\nc=a+b;\na = b;\nb = c;\n}\nreturn b;\n}\n}'
        }, 
        testCases: [
            {
                id: 1,
                weightage: 0.3,
                input: 0,
                expectedOutput: 0,
                visible: false
            },
            {
                id: 1,
                weightage: 0.2,
                input: 1,
                expectedOutput: 1,
                visible: true
            },
            {
                id: 1,
                weightage: 0.2,
                input: 20,
                expectedOutput: 6765,
                visible: true
            },
            {
                id: 1,
                weightage: 0.3,
                input: 10,
                expectedOutput: 55,
                visible: false
            }
        ]
    }
};