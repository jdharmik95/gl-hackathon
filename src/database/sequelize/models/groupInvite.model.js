const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('groupInvite', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		groupId: {
			allowNull: false,
			type: DataTypes.INTEGER,
			unique: false
		},
		userId: {
			allowNull: true,
			type: DataTypes.INTEGER,
			unique: false
		},
		isAccepted: {
            allowNull: false,
			type: DataTypes.BOOLEAN,
			default: false
		}
	},{
        uniqueKeys: {
            group_user_uniqueKey: {
                fields: ['groupId', 'userId']
            }
        }
    });
};
