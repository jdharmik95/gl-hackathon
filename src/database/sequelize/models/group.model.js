const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('group', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		createdByUserId: {
			allowNull: false,
			type: DataTypes.INTEGER,
			unique: false
		},
		name: {
			allowNull: true,
			type: DataTypes.STRING,
			unique: false
		},
		noOfMembers: {
            allowNull: false,
            default: 0,
			type: DataTypes.INTEGER,
			unique: false
		}
	});
};
