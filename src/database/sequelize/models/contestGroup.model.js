const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('contestGroup', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		contestId: {
			allowNull: false,
			type: DataTypes.INTEGER
		},
		score: {
			allowNull: false,
			type: DataTypes.INTEGER
		},
		groupId: {
			allowNull: false,
			type: DataTypes.INTEGER,
		}
	}, {
        uniqueKeys: {
            contest_group_uniqueKey: {
                fields: ['contestId', 'groupId']
            }
        }
    });
};
