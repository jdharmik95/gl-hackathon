const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('user', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		email: {
			allowNull: false,
			type: DataTypes.STRING,
			unique: true
		},
		firstName: {
			allowNull: true,
			type: DataTypes.STRING,
			unique: false
		},
		lastName: {
			allowNull: true,
			type: DataTypes.STRING,
			unique: false
		}
	});
};
