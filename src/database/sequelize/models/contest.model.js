const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('contest', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING
        },
		problemId: {
			allowNull: true,
			type: DataTypes.INTEGER,
			unique: false
		},
		description: {
			allowNull: false,
			type: DataTypes.TEXT
		},
		startAtUTC: {
			allowNull: true,
			type: DataTypes.DATE,
			unique: false
        },
		endAtUTC: {
			allowNull: true,
			type: DataTypes.DATE,
			unique: false
		}
	});
};
