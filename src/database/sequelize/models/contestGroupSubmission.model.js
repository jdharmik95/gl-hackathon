const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('contestGroupSubmission', {
		// The following specification of the 'id' attribute could be omitted
		// since it is the default.    Id, code_snippet_cdn_link,score,contest_group_id, created_at, updated_at
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		contestGroupId: {
            allowNull: false,
			type: DataTypes.INTEGER  
        },
        score: {
            allowNull: false,
			type: DataTypes.INTEGER 
        },
        code: {
            allowNull: false,
			type: DataTypes.TEXT 
        }
	});
};
