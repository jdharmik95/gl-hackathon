function applyExtraSetup(sequelize) {
	const { group,groupInvite,contest,contestGroup,contestGroupSubmission,user } = sequelize.models;

	groupInvite.belongsTo(group);
	groupInvite.belongsTo(user);
	group.hasMany(groupInvite);
	groupInvite.belongsTo(group);
	contest.hasMany(contestGroup);
	contestGroup.belongsTo(group);
	contestGroup.belongsTo(contest);
	contestGroup.hasMany(contestGroupSubmission);
	contestGroupSubmission.belongsTo(contestGroup);
}

module.exports = { applyExtraSetup };
