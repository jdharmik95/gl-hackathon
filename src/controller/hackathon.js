const { models } = require('../database/sequelize');
const codeEvaluator = require('../utils/codeEvaluator');
const sequelize = require('../database/sequelize');

async function evaluateCode(req, res) { 
    let payload = req.body;

    try{
        let score = await codeEvaluator(payload['problemId'],payload['code']);
        res.status(200).json({
            score: Math.round(score)
        });
    }catch(err){
        res.status(400).json({
            error: 'Error occurred while executing code: ' + err.message
        });
    }
};

async function submissions(req, res){
    let payload = req.body;
    let contest = models.contest;

    let contestGroup = await models.contestGroup.findOne({
        include: [
            {
                model: contest,
                required: true
            }
        ],
        where: {
            id: payload['contestGroupId']
        }
    });
    if(!contestGroup){
        res.status(400).json({
            error: 'Invalid contestGroupId given.'
        });
    }
    let computedScore = await codeEvaluator(contestGroup.contest.problemId,payload['code']);
	let contestGroupSubmission = await models.contestGroupSubmission.create({
        contestGroupId: payload['contestGroupId'],
        score: computedScore,
        code: payload['code']
    });
    let contestGroupMaxScore = await models.contestGroupSubmission.findOne({
        where: {
            contestGroupId: payload['contestGroupId']
        },
        attributes: [[sequelize.fn('max', sequelize.col('score')), 'maxScore']],
        raw: true
    });
    console.log(contestGroupMaxScore);
    let maxScore = contestGroupMaxScore['maxScore'];
    await models.contestGroup.update({
        score: maxScore
    },{
        where: { 
            id: payload['contestGroupId']
        }
    });
    console.log('max score', maxScore);
    res.status(200).json({
        'maxScore': maxScore,
        'submissionId': contestGroupSubmission.id,
        'contestGroupId': payload['contestGroupId']
    });
}

async function getLeaderboard(req, res) { 
    let contestId = req.params.contestId;
    let queryParams = req.query;
    let limit = queryParams['limit']?queryParams['limit']: 10;
    let offset = queryParams['offset']?queryParams['offset']: 0;
    let group = models.group;
    console.log(group);
    let contestGroupsCount = await models.contestGroup.count({
        where: {
            contestId
        }
    });
    let contestGroups = await models.contestGroup.findAll({
        include: [{
            model: group,
            required: true
        }],
        attributes: ['group.name','score','group.id'],
        where: {
            contestId
        },
        order: [
            ['score', 'DESC']
        ],
        limit,
        offset,
        raw: true
    })
    // ranking logic based on the overall scores captured
    let currentRank = null;
    let currentScore = null;
    contestGroups = contestGroups.map((cg)=>{
        if(!currentRank && !currentScore){
            currentScore = cg.score;
            currentRank = 1;
        }
        if(cg.score < currentScore){
            currentRank = currentRank + 1;
        }
        currentScore = cg.score;
        cg['rank'] = currentRank;
        return cg;
    })
    res.status(200).json({
        groups: contestGroups,
        total: contestGroupsCount,
        limit: Number(limit),
        offset: Number(offset)
    });
};
module.exports ={
    evaluateCode,
    getLeaderboard,
    submissions
};