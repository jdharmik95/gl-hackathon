# gl-hackathon

Doc link:- https://docs.google.com/document/d/1S_LG-ygwe18xGIDB1Hd0tb6RKvsmSLxa2mkS3SuBhO0/edit#

This is the coding part of the gl-hackathon problem discussed during interview.
## Instructions for setting up
1. Git clone the project in the your local PC. Navigate the project folder.
2. Run command `npm install`, this will install the project dependencies.
3. Next, Run command `npm run setupdb`, this will setup sqllite db in the current project folder and seed the mock data.
4. Next run, `npm run start`, this will start the express server running at http://localhost:8081.
5. Now, we can access the apis as mentioned below.

## Assumptions
1. According to the design mentioned in the doc, we basically will have code-execution service seperately, which will have problem statement, testcases stored as problemsCollection in mongodb. And the `Execute Code api` would be available from that service, For the simplicity of the current implementation, I have stored problemsCollection in mocks folder inside database and created api in the current service.

2. I have created a problem for printing nth number in fibannoci series in the problem collections inside [src/database/mocks/problemsCollection.js], problemId - 1

3. I have created only one contest in mock data - contestId - 1


## APIs

#### 1. Execute Code [POST]

URL:- http://localhost:8081/api/execute-code

This api takes in code and problemId and returns the computed score for the given inputs.

header:
```
{
    "apikey": 'DHv6bDPs5FcOdAhxYpRs'
}
```

sample request payload:-
```
{
    "code": "function(n){return 1;}",
    "problemId": 1
}
```
sample response:-
```
{
    score: 10
}
```

#### 2. Leaderboard [GET]

URL:- http://localhost:8081/api/contest/1/leaderboard?limit=10&offset=0

This api returns the groups with top scores in a paginated manner, default limit is 10 and default offset is 0.
Here we have rank computed based on the score of the contest groups. If two groups are having same score, we are assigning we will be rank.

header:
```
{
    "apikey": 'DHv6bDPs5FcOdAhxYpRs'
}
```

sample response:-
```
{
    "groups": [
        {
            "name": "Vagram",
            "score": 99,
            "id": 6,
            "group.id": 6,
            "group.createdByUserId": 77,
            "group.name": "Vagram",
            "group.noOfMembers": 3,
            "group.createdAt": "2020-09-03 16:52:01.729 +00:00",
            "group.updatedAt": "2020-09-03 16:52:01.729 +00:00",
            "rank": 1
        }],
    limit: 1,
    offset: 0
    total: 50
}
```
#### 3. Submissions [POST]

This is api will accept the solution with contestGroupId mentioned in paylaod. Firstly, it would execute the code and generate the score and create a submission record for given contestGroup, after that since one contestGroup can do multiple submissions,highest score among the individual submissions(contestGroupSubmissions) of that contestGroup would be updated as actual `contestGroup(score)`.

This will enable us to have record of individual submissions and their scores for given contestGroup, and overall best score is `pre-computed` in contestGroup table for the group, so that when it comes to accessing leaderboard realtime, it would be faster when fetching leaderboard for even for thousands or millions of contestGroups.

URL:- http://localhost:8081/api/submissions

header:
```
{
    "apikey": 'DHv6bDPs5FcOdAhxYpRs'
}
```

sample request payload:-
```
{
    "code": "function(n){return 1;}",
    "contestGroupId": 12
}
```

sample response:-
```
{
    "maxScore": 24,
    "submissionId": 101,
    "contestGroupId": 12
}
```

